be sure to adds what is below to the application.properties file in the spring boot directory 

spring.sql.init.platform=postgres
spring.datasource.url=jdbc:postgresql://postgres_db:5432/postgres
spring.datasource.username=compose-postgres
spring.datasource.password=compose-postgres
spring.datasource.driver-class-name=org.postgresql.Driver